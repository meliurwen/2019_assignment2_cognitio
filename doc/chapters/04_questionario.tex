
\section{Linee guida adottate}

Ogni parola è importante quando si cerca di creare un \textit{questionario efficace}. Molti autori di questionari scrivono come pensano, credendo che questa sia una strategia efficace.
\newline
Questo ragionamento è pericoloso e può portare ad una conclusione errata. Infatti, se da una parte è vero che ogni tipo di comunicazione deve essere chiara e di facile comprensione, dall’altra si tratta di un’operazione spesso più complessa di quanto possa sembrare.
\newline
Difatti spesso capita di avere a che fare con stakeholder che non abbiano le idee chiare riguardo al progetto commissionato; alcuni sono genuinamente convinti di averle ma in realtà non è così. Una pillola che viene spesso viene sentita da persone tecniche che devono intefacciarsi con stakeholders non tecnici è la seguente:
    \begin{leftbar}
      \noindent\textbf{Un caso estremamente diffuso di cliente è di questo tipo:}\newline Cliente ti dice che vuole \textbf{\textit{A}} descrivendoti \textbf{\textit{B}}, ma in realtà quello che realmente vuole (ma non lo sa) è \textbf{\textit{C}}.
      \end{leftbar}
Per cui si è deciso di stendere alcune linee guida per la stesura del questionario, in modo che si possano ottimizzare l'efficacia e di conseguenza la qualità dei risultati ottenuti.
\newline
Nel caso della stesura di un questionario il rischio che l'intervistato interpreti erroneamente le domande e fornisca risposte parziali, se non addirittura errate, deve essere azzerato se si vogliono garantire all'indagine risultati reali. Al contempo, bisogna far sì che la persona a cui è stato somministrato il questionario si senta coinvolta ed interessata per evitare che scelga di abbandonare il sondaggio od addirittura di rispondere in maniera poco attenta.
\newline
\newline
Le linee guida sono elencate di seguito:

\begin{enumerate}
    \item \textbf{Togliere ogni campo libero}.
    \item \textbf{Togliere quanti più tecnicismi possibile ove non necessario}, confonde gli stakeholder e tende a farli entrare in un campo di decisioni che dovrebbe competere solo a noi.
    \item \textit{Stesso discorso} del punto precedente \textit{per} quanto riguarda \textit{la specificità delle domande}.
    \item \textbf{Dare un ordine sensato alle domande}, di solito si va in ordine da domanda estremamente generica a più specifica man mano si procede con il questionario.
    \item Per ottenere informazioni specifiche che gli stackeholder possiedono ma si presume che non siano in grado di fornire in via diretta per mancanza di di conoscenze tecniche; può essere utile \textbf{fare domande di circostanza}.
    \begin{displayquote}
      \textbf{Esempio:}\newline
      \textbf{[Q] Quanti anni fa hai acquistato la tua ultima auto?}\newline
  Con questa domanda posso dire con una certa confidenza quale standard di emissioni appartengono i veicoli guidati dai membri del mio pool (Euro 1, 2, 3, ecc...).
    \end{displayquote}
    \item Può essere utile \textbf{spezzare il questionario} in una successiva intervista la quale viene generata in base ai risultati ottenuti nel primo.
    \item \textbf{Parte del questionario} deve essere \textbf{dedicata al tipo di stakeholder intervistato} con domande che tengano conto della categoria in oggetto.
\end{enumerate}

\section{Organizzazione}

Il questionario è stato soministrato agli stakeholder appartenenti alla macrocategoria degli \textbf{Utenti} alla quale è stata dedicata una sezione con domande dedicate per ogni sottocategoria (Studenti, Genitori ed Insegnanti).\newline
L'ordine delle sezioni e delle domande contenute non è casuale, sono rispettivamente disposte dalla più generica a quella meno e dalla più a meno tecnica. Un questionario ordinato in questa maniera ci permette di avere risposte del candato più attente, suscitadogli anche la sua curiosità andando così ad aumentare le probabilità che accetti, magari di anche di buon grado, di essere intervistato.\newline

La struttura del questionario è suddivisa in tre sezioni, tra le quali una è composta da tre varianti:
\begin{itemize}
  \item \textbf{Domande Generiche:} Sono domande di carattere comune da somministrare a tutti gli utenti, il cui scopo principale, oltre alle motivazioni che verranno elecate nei prossimi paragrafi, sarebbe dare una prima inquadratura delle varie sottocategorie di intervistati e trovare una base comune da cui far partire lo sviluppo dell'applicazione. Un esempio sono le domande inerenti alle piataforme utilizzate ed i sistemi operativi.
  \item \textbf{Domande Utenti:} È una sezione composta da tre varianti, una per ogni tipologia di Utente. Ogni variante dovrà essere svolta esclusivamente dalla categoria di Utente assegnata. Tutte e tre le varianti hanno domande orientate a carpire informazioni principalmente riguardanti le prferenze e le abitudini delle categorie di utenti intervistati.\newline
  Le varianti citate sono in seguito elencate:
  \begin{itemize}
      \item \textbf{Domande per Insegnanti}
      \item \textbf{Domande per Studenti}
      \item \textbf{Domande per Genitori}
  \end{itemize}
  \item \textbf{Domanda Intervista} Questa parte è composta da un'unica domanda ed è rivolta a tutti gli Utenti. Essa consiste nel chiedere di lasciare un contatto in caso si fosse disposti a essere sottoposti ad un'intervista, prima della quale verrà somministrato un prototipo dell'applicazione da testare.
\end{itemize}

\section{Modalità di somministrazione}

Il questionario è stato \textit{somministrato per via cartacea} ed è stato consegnato agli insegnanti i quali si sono resi disponibili ad \textit{inoltrare} le altre due varianti ai propri studenti e relativi genitori (vedasi workflow del capitolo precedente).\newline
L'alternativa alla via analogica sarebbe stata quella elettronica tramite un servizio cloud di terze parti (\textit{"Google Moduli"}), la quale avrebbe potuto portare problemi sia nella diffusione che nello svolgimento del questionario. I problemi di tale approccio sono emersi durante una prima riunione informale con un ristretto gruppo di insegnanti i quali oltre ad esprimere dubbi su un'utilizzo attento e non problematico di tale strumento, hanno espresso anche dubbi riguardante \textit{la privacy dei dati} trattati, che sarebbero stati elaborati e stoccati su un'applicazione cloud di terze parti. Inoltre tutti gli insegnanti contattati si sono resi ben disposti ad inoltrare i questionari.\newline
\newline
La scelta analogica ha quindi come non trascurabile vantaggio di raggiungere una capillarità di copertura quasi totale degli studenti e genitori potenzialmente coinvolgibili, oltre che ottenere risposte più genuine. Lo svantaggio sarebbe che una volta raccolti tutti i questionari si rende necessario un'altro non trascurabile compito, ossia trascrivere a mano i dati a macchina, per poi essere elaborati tramite un foglio di calcolo.

\section{Struttura}

\begin{figure}[H]
\center
    \includegraphics[width=0.8\linewidth]{img/questionari.pdf}
    \caption{Struttura del Questionario}
  \label{fig:coffee}
\end{figure}

\section{Domande Generiche}
\begin{enumerate}
    \item \textbf{Cosa usi di più?}
    \begin{itemize}
        \item \textit{PC/Portatile}
        \item \textit{Cellulare/Tablet}
    \end{itemize}
    
    Serve a capire a che tipo di piattaforma (mobile o pc) è necessario focalizzare lo sviluppo dell'applicazione.
    
    \item \textbf{Che tipo di cellulare/tablet usi?}
    \begin{itemize}
        \item \textit{Apple (iPhone, iPad)}
        \item \textit{Altro}
    \end{itemize}
    
    Analogamente a sopra, serve a capire a che sistema operativo mobile concentrarsi di più, almeno per le prime versioni dell'applicazioni. La domanda è binaria e chiede specificatamente di prodotti Apple in quanto, i suoi dispositivi sono riconosciuti tramite il brand, mentre non è così per i prodotti Android che possono avere più brand e l'aggiunta di una tale opzione avrebbe confuso utenti poco avvezzi con la tecnologia. L'aggiunta di altri sistemi operativi sarebbe stato fuorviante e la fetta di mercato che occupano così trascurabile da poter assumere che "se non si usa iOS allora si usa Android".

    \item \textbf{Quanti anni hai?}
    
    \begin{itemize}
        \item \textit{.......}
    \end{itemize}
    
    Serve a capire che tipo di utente si ha a che fare.

    \item \textbf{Dove abiti?}
    \begin{itemize}
        \item \textit{A Brescia}
        \item \textit{In un paese con meno di 1000 abitanti}
        \item \textit{In un paese con 1000-5000 abitanti}
        \item \textit{In un paese con 5000-10 000 abitanti}
        \item \textit{In un paese con più di 10 000 abitanti}
    \end{itemize}
    
    Serve per capire la capacità che ha un insegnante di essere assorbito dal mercato e per quanto rigaurda gli studenti e genitori di trovare un insegnante. Può essere utile al team dedicato al marketing ed alle pubbliche relazioni per ottimizzare la diffusione e promozione dell'applicazione.
    
    \item \textbf{Quanto sei distante dalla città?}
    \begin{itemize}
        \item \textit{0-15 minuti}
        \item \textit{16-30 minuti}
        \item \textit{31-60 minuti}
        \item \textit{più di 60 minuti}
    \end{itemize}
    
    La città è un polo con una grande concentrazione di abitanti, quindi probabilmente con anche un grande potenziale bacino di utenza. Si vuole capire, come sopra, com'è distribuita e se c'è un gradiente. In ogni caso può essere utile nello sviluppo dell'algoritmo di matching ed al team PR e marketing per la promozione dell'app. Si vuole capire in particolare quanto potrebbe incidere la distanza nell'uso dell'app.
    

\end{enumerate}


\section{Domande per Studenti}

\begin{enumerate}
    
    \item \textbf{Che scuola frequenti?}
    
    \begin{itemize}
        \item \textit{Elementari}
        \item \textit{Medie}
        \item \textit{Superiori}
        \item \textit{Università}
        \item \textit{Altro (specifica).......}
    \end{itemize}
    
    Ci serve a capire se la figura di insegnante richiesto è più una di quelle di un insegnante specializzato in materia (più è alto il grado di studio) o più una figura educativa (più è basso il grado di studio). Aiuta anche a capire su quali funzionalità più pertinenti ad un grado di studio rispetto ad un altro concentrarsi di più durante lo sviluppo.
    
    \item \textbf{Che tipo di scuola di secondo grado frequenti (risondi solo se la frequenti)?}
    \begin{itemize}
        \item \textit{Liceo}
        \item \textit{Istituto tecnico}
        \item \textit{Istituto professionale}
    \end{itemize}
    
    Analogo alla domanda precedente, ma con l'aggiunta che si vuole sapere se c'è richiesta di materie molto specializzate e tecniche. In tal caso si possono aggiungere funzionalità più adatte a determinati tipi di istituti.
    
    
    \item \textbf{Con che frequenza prendi ripetizioni?}
    \begin{itemize}
        \item \textit{Molto spesso}
        \item \textit{Ogni tanto}
        \item \textit{Raramente}
        \item \textit{Non ho mai preso ripetizioni prima d'ora}
    \end{itemize}
    
    Serve a capire la frequenza con cui potrebbe essere utilizzata l'applicazione in modo da capire se concentrarsi di più su o meno su requisiti legati al sistema di notifiche, esperienza utente e consumo energetico (in caso si vada a sviluppare su piattaforma mobile).
    
    \item \textbf{In che occasioni cerchi qualcuno per darti ripetizioni?}
    \begin{itemize}
        \item \textit{Per prepararmi a verifiche od interrogazioni}
        \item \textit{Per tenermi preparato/a}
        \item \textit{Per fare i compiti}
        \item \textit{Per farmi rispiegare lezioni che non ho capito}
    \end{itemize}
    
    Serve per capire se quali funzionalità concentrarsi od aggiungere, specialmente legate alla pianificazione ed alla comunicazione con l'insegnante. Per esempio se l'opzione "verifiche" risultasse particolarmente marcata si potrebbe sviluppare una funzionalità di calendario per avvisare a tutti gli utenti (studente, genitore e insegnante) della verifica imminente e dell'agginta di una funzionalità invio della foto delle verifiche all'insegnante.
    
    \item \textbf{In quali fasce orarie prendi ripetizioni di solito?}
    \begin{itemize}
        \item \textit{Mattina}
        \item \textit{Primo pomeriggio}
        \item \textit{Tardo pomeriggio (16)}
        \item \textit{Sera (dalle 19:00 in poi)}
    \end{itemize}
    
    È utile a capire se è necessario introdurre opzioni e funzionalità legate all'orario di disponibilità dell'insegnante e dello studente. Per esempio se si vede che gli studenti tendono a concentrarsi su una sola fascia oraria potrebbe essere utile inserire una funzionalità di negoziazione e schedulazione automatica delle lezioni tra studente ed insegnante in modo da massimizzare il numero di lezione date per insegnante (specialmente per quelli particolarmente impegnati) ed al contempo soddisfare lo studente.
    
    \item \textbf{In quale categoria di materie preferisci essere aiutato?}
    \begin{itemize}
      \item \textit{Scientifiche}
      \item \textit{Scientifico-tecniche}
      \item \textit{Umanistiche}
    \end{itemize}
    
    Per capire che tipo di insegnante attrarre di più per l'utilizzo dell'app. Anche per capire su che funzionalità dell'app affini alla categoria di materia concentrarsi.

    \item \textbf{Hai delle certificazioni DSA?}
    \begin{itemize}
      \item \textit{Si}
      \item \textit{No}
    \end{itemize}
    
    Serve per capire se serve concentrarsi su requisiti affini ad un tipo di utenza affetta da Disturbi Specifici dell'Apprendimento e se serve concentrarsi su attrarre all'utilizzo dell'app insegnanti preparati per questi tipi di disturbi. 

    \item \textbf{Preferisci lezioni singole o di gruppo?}
    \begin{itemize}
      \item \textit{Singole}
      \item \textit{Di gruppo}
    \end{itemize}
    
    Serve per capire se concentrarsi su funzionalità affini alla composizione dei gruppi (se formati da una persona o più). Per esempio se una percentuale non trascurabile dovesse rispondere "di gruppo" ci si potrebbe concentrare sul miglioramento dell'algoritmo di schedulazione e su quello di matching, aggiungendo anche la possibilità di accordarsi con altri studenti sul fare lezione di gruppo.

\end{enumerate}

\section{Domande per Insegnanti}

\begin{enumerate}
    
    \item \textbf{Da quanto tempo dai ripetizioni?}
    \begin{itemize}
        \item \textit{0-6 mesi}
        \item \textit{7-11 mesi}
        \item \textit{1 anno}
        \item \textit{2 anni}
        \item \textit{3 anni o più}
    \end{itemize}
    
    Serve per capire la preparazione del pool degli insegnanti e per quanto sono disposti ad insegnare. In questo modo ci si può regolare su funzionalità legate al tempo, per esempio alla prenotazione e schedulazione delle lezioni, dove si potrebbe impedire di prenotare con un aticipo di "troppi" mesi.

    \item \textbf{Quanto tempo dedichi a dare ripetizioni a setitmana?}
    \begin{itemize}
        \item \textit{meno di 5 ore}
        \item \textit{6-15 ore}
        \item \textit{16-30 ore}
        \item \textit{31-45}
        \item \textit{più di 45 ore}
    \end{itemize}
    
    Analogamente a sopra, serve anche ad inferire se si tratta di insegnanti "professionisti" o saltuari.

    \item \textbf{Quali servizi legali utilizzi per offrire ripetizioni?}
    \begin{itemize}
      \item \textit{Vengo pagato da cooperative}
      \item \textit{Ho la Partita IVA}
      \item \textit{Lavoro per una azienda che offre ripetizioni}
      \item \textit{Lavoro in nero}
    \end{itemize}
    
    Serve a capire se è utile per il mercato e per gli stakeholder legali sviluppare un sistema di contratti e pagamenti gestiti dall'applicazione. Per esempio se conviene sviluppare un business model simile ad Uber.

\end{enumerate}


\section{Domande per Genitori}

\begin{enumerate}

    \item \textbf{Mettere in ordine dal più al meno importante la qualità più decisiva del proprio insegnante:}
    \begin{itemize}
      \item \textit{Prezzo}
      \item \textit{Vicinanza}
      \item \textit{Disponibilità a spostarsi}
      \item \textit{Preparazione nella materia}
    \end{itemize}
    
    È utile per capire la scrematura degli insegnanti che dobbiamo effettuare per servire al meglio l'utenza. Difatti un'utenza che si dimostra molto attenta al prezzo può indicarci di stare indicare di spingerci ad accettare insegnanti meno preparati ma con prezzi inferiori, col vantaggio di avere una maggiore copertura sul territorio. Questo può anche aiutare a migliorare certi aspetti dell'algoritmo di matching.
    
    \item \textbf{Quanto spendete mediamente all'ora per le leziondi di vostro figlio?}
    \begin{itemize}
      \item \textit{fino a 5€}
      \item \textit{6€-10€}
      \item \textit{11€-17€}
      \item \textit{18€-29€}
      \item \textit{più di 30€}
    \end{itemize}
    
    Come nel punto precedente.
    
\item \textbf{Quale modalità di comunicazione con l'insegnate utilizzate?}
\begin{itemize}
\item \textit{Telefonate}
\item \textit{Servizi di messaggistica istantanea}
\item \textit{Comunicazioni Verbali}
\end{itemize}

Utile per capire se può essere utile lo sviluppo di una struttura di messaggistica interna o un plugin di chiamate Voip.
    
\end{enumerate}

\section{Domanda Intervista}

\begin{enumerate}

    \item \textbf{Saresti disposto a provare in esclusiva un prototipo dell'applicazione? Dopo la prova ti faremo alcune domande sull'esperienza avuta. Se sei interessato lascia un tuo contatto (telefono, email, ecc...).}
    \begin{itemize}
      \item ................
    \end{itemize}
    
    Serve per contattare l'untente al fine di organizzare la prova dell'app seguita da un'intervista.
    
\end{enumerate}

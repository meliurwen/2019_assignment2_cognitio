<div align="center">
<img src="doc/img/cognitio_logo.png" alt="Cognitio Logo" width="200" >
</div>

# 2019 Assignment 2

> **_Note:_** È una repo privata, usa l'utente `sw_dev_process_course` per accederci!

+ **_Link del Progetto:_** https://gitlab.com/meliurwen/2019_assignment2_cognitio
+ **_Link del Documento in PDF:_** https://gitlab.com/meliurwen/2019_assignment2_cognitio/blob/master/assignment2_cognitio.pdf
+ **_Link dei dati in PDF:_** https://gitlab.com/meliurwen/2019_assignment2_cognitio/blob/master/dati.pdf

## Applicazione

L'applicazione oggetto di questo assigment è **_Cognitio_**.

Si tratta di un'applicazione in grado di mettere in contatto persone che _offrono ripetizioni_ con _studenti_ che ne sono alla ricerca. Essa oltre ad occuparsi di facilitare il _"primo contatto"_ sarà in grado di _gestire_ l'andamento delle lezioni, tenendo traccia di _appuntamenti_, eventuali _compiti assegnati_ e _note_.

Allo stato attuale l'applicazione è stata pensata per essere utilizzata nell'area metropolitana di _Brescia_ e _sud della provincia_ della medesima città, con un target iniziale ristretto agli studenti delle scuole primarie e secondarie.

A seconda della trazione che potrebbe ricevere una volta lanciata, si potrà valutare un'eventuale _espansione_ sia verso un'utenza _universitaria_ che del territorio coperto.

## Membri del Gruppo

Salanti Michele - 793091